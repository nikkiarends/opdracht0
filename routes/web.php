<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/homepage', function () {
    return view('homepage');
});


//other
Route::get('/dashboard', 'PostController@myposts');
Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
//posts
Route::get('/posts/create', 'PostController@create');
Route::get('/posts', 'PostController@index');
Route::post('/posts', 'PostController@store');
Route::get('/posts/{id}/edit', 'PostController@editpost');
Route::post('/posts/{id}/edit', 'PostController@updatepost');
Route::get('/post/{id}/delete', 'PostController@deletepost')->name('post.delete');
//comments
Route::get('/posts/{post}', 'PostController@viewpost')->name('posts');
Route::post('/posts/{id}/comment', 'PostController@storecomment' );
Route::get('/posts/{id}/comment', 'PostController@comment');
Route::get('/posts/{id}/comment/delete', 'PostController@deletecomment')->name('comment.delete');
//users
Route::get('/users', 'UserController@index');
Route::post('/users', 'UserController@store');
Route::get('/users/create', 'UserController@create');
Route::get('/users/edit', 'UserController@edituser');
Route::get('/users/{id}/edit', 'UserController@adminedituser')->name('user.edit');
Route::post('/users/edit', 'UserController@updateuser');
Route::post('/users/{id}/edit', 'UserController@adminupdateuser');
Route::get('/users/{id}/delete', 'UserController@deleteuser')->name('user.delete');
//groups
//Route::get('/groups', 'GroupsController@index');
//Route::post('/groups', 'GroupsController@store');
//Route::get('/groups/create', 'GroupsController@create');


Auth::routes();

