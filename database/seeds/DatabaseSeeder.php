<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    public function run()

    {   
        // create users

        //admin
        $admin = new \App\User();
        $admin->username = 'Nikki';
        $admin->email = 'admin@example.com';
        $admin->avatar = 'https://scontent.fams1-2.fna.fbcdn.net/v/t1.0-9/28685186_1095589540583821_3182759876294039657_n.jpg?_nc_cat=101&_nc_ht=scontent.fams1-2.fna&oh=e2ab5ed36d7f48e5f1c8b0253cdb6555&oe=5D2052C2';
        $admin->password = bcrypt('cleo24');
        $admin->save();
        
        
        //mod
        $mod = new \App\User();
        $mod->username = 'Ben';
        $mod->email = 'mod@example.com';
        $mod->password = bcrypt('secret');
        $mod->save();
        
    
        //user Naya
        $user = new \App\User();
        $user->id = '3';
        $user->username = 'Naya';
        $user->email = 'naya@example.com';
        $user->password = bcrypt('naya');
        $user->avatar = 'https://cdn1.picsart.com/1110379354.jpeg?c256x256';
        $user->save();
        
        // create permissions
        
        $delete_posts_permission= new \App\Permission();
        $delete_posts_permission->name = 'Can delete posts';
        $delete_posts_permission->save();
        
        $delete_users_permission= new \App\Permission();
        $delete_users_permission->name = 'Can delete users';
        $delete_users_permission->save();

        $edit_posts_permission = new \App\Permission();
        $edit_posts_permission->name = 'Can edit posts';
        $edit_posts_permission->save();
        
        $edit_users_permission= new \App\Permission();
        $edit_users_permission->name = 'Can edit users';
        $edit_users_permission->save();

        
        // create groups

        $admin_role = new \App\Group();
        $admin_role->name = 'Administrator';
        $admin_role->save();

        $mod_role = new \App\Group();   
        $mod_role->name = 'Moderator';
        $mod_role->save();
       
        $user_role = new \App\Group();
        $user_role->name = "User";
        $user_role->save();

        // attach users to groups
        $admin->groups()->attach($admin_role);
        $mod->groups()->attach($mod_role);
        $user->groups()->attach($user_role);

        // attach permissions to groups
        $admin_role->permissions()->attach($delete_posts_permission);
        $admin_role->permissions()->attach($delete_users_permission);
        $admin_role->permissions()->attach($edit_posts_permission); 
        $admin_role->permissions()->attach($edit_users_permission);
        $mod_role->permissions()->attach($edit_posts_permission);
      
        // Create posts
        $post1 = new \App\Post();
        $post1->title = 'My first post';
        $post1->message = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.
        // Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia.';
        $post1->user_id = '1';
        $post1->save();

        $post2 = new \App\Post();
        $post2->title = 'Hello!';
        $post2->message = 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui.
        // Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc, quis gravida magna mi a libero. Fusce vulputate eleifend sapien. Vestibulum purus quam, scelerisque ut, mollis sed, nonummy id, metus. Nullam accumsan lorem in dui. Cras ultricies mi eu turpis hendrerit fringilla. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; In ac dui quis mi consectetuer lacinia.';
        $post2->user_id = '2';
        $post2->save();
    }
}
