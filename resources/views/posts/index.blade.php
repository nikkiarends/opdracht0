@extends('layout')
<style>
</style>
@section('title', 'Posts')
@section('content')
<center>
<div class="block">
<h2>Posts</h2>

<form action="/posts/create">
<button class="button">+ Create new post</button>
</form>

@foreach ($posts as $post) 

<table class="post">

<tr>
  <td class="posttitle">{{ $post->title  }}</td>
</tr>

<tr>
<td> Posted by: {{ $post->user->username }} </td>
</tr>

<div>
  <tr>
    <td class="posttext">{{ $post->message }}</td>
  </tr>
</div>
<tr>
<td ><a class="viewpost" href="/posts/{{ $post->id }}">View this post and comment</a></td>
</td>

<tr>
  <td class="date">Posted at {{ $post->created_at }}</td>
</tr>


@endforeach




</center>


@endsection
