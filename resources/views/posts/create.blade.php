@extends('layout')
<style>
.message {
    height: 400px;
    width: 600px;
    font-family: 'Quicksand', sans-serif;
    padding: 12px 20px;
    margin: 5px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
    border-radius: 12px;
     }
.title {
    width: 400px;
}

.create {
    width: 200px;
}



</style>
@section('title', 'Create post')
@section('content')
<center>
<div class="block">
<h2>Create Post</h2>
@if(Auth::guest())
<p>You need to <a href="\login">sign in</a> or <a href="\users/create">register</a> to view create a post!</p>

@else

@if ($errors->any())
    <div class="notification">
        <ul>
        @foreach ($errors->all() as $error)
            <li style="list-style-type:none;"><strong>Warning! </strong>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif

<form action="/posts" method="POST" id="messageform">

@csrf

<table>
    <tr>
        <td>Title: </td>
        </tr>
        <tr>
        <td><input class="title" type="text" name="title"><td>
    </tr>
<tr><td>Message : </td></tr>
    <tr>
        
      <td>
      <textarea class="message" rows="4" cols="50" name="message" form="messageform"></textarea>
         </td> </tr>
    
    <tr>
        <td><input class="button create" type="submit" name="submit" value="Add"><td>
    </tr>

</table>
</form>

@endif
</center>
</div>
@endsection