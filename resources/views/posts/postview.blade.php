@extends('layout')
@section('title', 'Post overview')

@section('content')
<center>
<div class="block">

{{-- @foreach ($posts as $post)  --}}

<table class="post">



<div class="usericon">
  <tr>
    <td><img class="useravatar" src="{{ $post->user->avatar }}" ></td>
  </tr>

  <tr>
    <td class="username">{{ $post->user->username }}</td>
  </tr>
</div>

<tr class="posttitle">
  <td>{{ $post->title  }}</td>
</tr>


  <tr class="posttext">
     <td>{{ $post->message }}</td>
  </tr>



<tr>
<td>
<a class="commentbutton button" href="{{url('/posts/'.$post->id.'/comment')}}">+ Add comment</a> 
<!-- specify if the user logged in can edit the post -->
@can('update', $post)
  <a class="commentbutton button" href="{{url('/posts/'.$post->id.'/edit')}}">Edit post</a>
@endcan
<!-- specify if the user logged in can delete the post -->
@can('delete', $post)
  <a class="commentbutton button" href="{{ route('post.delete', ['id' => $post->id]) }}">Delete post</a>
@endcan
</td>
</tr>

<tr>
  <td class="date">Posted at {{ $post->created_at }}</td>
</tr>

<br>

@foreach ($post->comments as $comment)
  <table class="comment">
    <div class="usericon">
      <tr>
        <td><img class="useravatar" src="{{ $comment->user->avatar }}"></td>
      </tr>
      <tr>
        <td class="username">{{ $comment->user->username }}</td> 
      </tr>
  </div>

<tr>
  <td class="commenttitle">{{ $comment->title }}</td>
</tr>

<tr>
  <td>{{ $comment->message }}</td>
</tr>
<tr>
<td>

@can('update', $comment)
<!-- specify if the user logged in can edit the comment -->
  <a class="commentbutton button" href="{{url('/posts/'.$comment->id.'/edit')}}">Edit comment</a>
@endcan
<!-- specify if the user logged in can delete the comment -->
@can('delete', $comment)
  <a class="commentbutton button" href="{{ route('comment.delete', ['id' => $comment->id]) }}">Delete comment</a>
@endcan
</td>
</tr>

<tr>
  <td class="date">Posted at {{ $comment->created_at }}</td>
</tr>

</table>


@endforeach

{{-- @endforeach --}}


</center>
</div>
@endsection
