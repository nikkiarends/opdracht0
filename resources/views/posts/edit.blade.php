@extends('layout')
<style>
.message {
    height: 400px;
    width: 600px;
    font-family: 'Quicksand', sans-serif;
    padding: 12px 20px;
    margin: 5px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
    border-radius: 12px;
     }
.title {
    width: 400px;
}

</style>
@section('title', 'Editor')
@section('content')
<center>
<div class="block">
<br>
<h1 class="title">Edit post</h1>

<form method="post" action="">
	@csrf
<table>
    <tr>
        <td>Title: </td>
        </tr>
        <tr>
        <td><input type="text" class="title" name="title" placeholder="Title" value="{{ $post->title }}"></td>
    </tr>
    <tr>
    </tr>
<tr><td>Message : </td></tr>
    <tr>
        
      <td>
      <textarea name="message" class="message" rows="7" cols="50">{{ $post->message }}</textarea>
         </td> </tr>
    
    <tr>
        <td><input class="button create" type="submit" name="submit" value="Edit"><td>
    </tr>
    <tr>
        <td><a href="{{ route('post.delete', ['id' => $post->id]) }}">
                        <button type="button" class="button">Delete Post</button>
                    </a><td>
    </tr>

    
</table>
	
<form action="/posts">
	<input class="button" type="submit" value="Back to posts" />
	</form>
	<br>
    </div>
</center>
@endsection
