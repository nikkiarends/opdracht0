
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>@yield('title')</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">

<style>
  /* general styling */
  body {
    width: 100%;
    background-color: #fff;
    color: #636b6f;
    font-family: 'Quicksand', sans-serif;
    font-weight: 200;
    background: #FEAC5E;  /* fallback for old browsers */
    background: -webkit-linear-gradient(to right, #4BC0C8, #C779D0, #FEAC5E);  /* Chrome 10-25, Safari 5.1-6 */
    background: linear-gradient(to right, #4BC0C8, #C779D0, #FEAC5E); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    background-size: 100%;
    background-color: rgba(238, 238, 238, 0.8) !important;
    background-blend-mode: color;
    width: 100%;
  }
    
  h2 {
    text-transform: uppercase;
  }

  input[type="text"], [type="password"] {
    font-size: 15px;
    border: 1px rgb(255, 102, 102, 0.1) solid;
    font-family: 'Quicksand', sans-serif;
    font-weight: 200;
    border-radius: 12px;
    height: 30px;
    padding: 4px;
    background-color: rgb(255,255,255);
  }

  select {
    font-size: 15px;
    border: 1px rgb(255, 102, 102, 0.1) solid;
    font-family: 'Quicksand', sans-serif;
    font-weight: 200;
    border-radius: 12px;
    height: 30px;
    width: 168px;
    padding: 4px;
    background-color: rgb(255,255,255);
  }

  /* button class */
  .button {
      font-size: 15px;
      border-radius: 12px;
      border:none;
      padding: 12px;
      font-family: 'Quicksand', sans-serif;
      font-weight: 200;
      background-color: rgb(115, 191, 234, 0.2); 
      text-decoration: none; 
      color: black;
  }

  .button:hover {
      background-color: rgb(115, 191, 234, 0.4);
      color: rgb(82, 118, 155, 1)
  }

  .logoutbutton {
      font-size: 12px;
      border-radius: 12px;
      border:none;
      padding: 12px;
      font-family: 'Quicksand', sans-serif;
      font-weight: 200;
      background-color: rgb(255, 102, 102, 0.1); 
      text-decoration: none; 
      color: white;
      
  }
  /*general basic display of a user icon*/
  .avatar {
    width: 50px;
  }

  /*styling in this sheet (including the menu and login bar)*/
  /*the block in which the information on the page is displayed*/
  .block {
    background-color:rgba(255,255,255,0.4);
    width: 1200px;
    height: 3000px;
    padding: 10px;
    padding-top: 20px;
    border-radius: 12px;
    margin-top: 15px;
    border: 1px rgb(255, 102, 102, 0.1) solid;
    padding-bottom: 20px;
  }

  /*loginbar on the top right */
  .topnav .login-container  {
    float: right; 
    padding-top: 10px;
    position: relative;
    right: 50px;
  }

  /*the text in the login bar*/
  .logintext {
    font-size: 15px;
    border: 1px rgb(255, 102, 102, 0.1) solid;
    font-family: 'Quicksand', sans-serif;
    font-weight: 200;
    border-radius: 12px;
    height: 30px;
    background-color: rgba(255,255,255,0.4);
  }

  /*color change of login text when clicked on*/
  .logintext:focus {
    border: 1px pink solid;
    background-color: rgb(255, 204, 153, 0.2);
  }

  .notification {
    padding: 20px;
    background-color: rgba(242, 85, 85, 0.4);
    border: 3px rgba(242, 85, 85, 0.6) solid;
    color: black;
    margin-bottom: 15px;
    width: 600px;
    list-style-type: none;
    border-radius: 12px;
  }

  /*post block layout*/
  .post{
    background-color:rgba(255,255,255,0.4);
    width: 1000px;
    padding: 10px;
    padding-top: 20px;
    border-radius: 12px;
    margin-top: 15px;
    border: 1px rgb(115, 191, 234, 0.1) solid;
    position: relative;
  }
    
  /* displays the date of posting at the right bottom */
  .date {
      font-size: 14px;
      text-align: right;
      color: rgb(82, 118, 155, 0.7);
  }

  /*title of the post*/
  .posttitle {
      text-align: left;
      color: #73BFEA;
      font-weight: bold;
      font-size: 20px;
      
  }

  .posttext {
    padding: 5px;
   
    border-radius: 12px;
  }
  /*user's avatar*/
  .useravatar {
      width: 50px;
      position: absolute;
      left: -75px;
      border-radius: 12px;
  }
  /*user's name*/
  .username {
      position: absolute;
      top: 75px;
      left: -75px;
      font-weight: bold;
  }
  /* brings the button to add a commment to the right of the post*/
  .commentbutton {
      float: left;
      margin: 3px;
      margin-top: 20px;
  }
  /*comment layout*/
  .comment {
    background-color:rgba(115, 191, 234, 0.1);
    width: 900px;
    padding: 10px;
    padding-top: 20px;
    border-radius: 12px;
    margin-top: 15px;
    margin-left: 100px;
    border: 1px rgb(115, 191, 234, 0.1) solid;
    position: relative;
  }
  /*comment title layouts*/
  .commenttitle {
    font-weight: bold;
  }

  .tinyavatar {
    height: 20px;
  }

  .avatarbig {
    height: 200px;
    border-radius: 12px;
  }

  .loggedinuser {
    padding: 4px;
    position: flex;
    left: -40px;
  }
  /* Top navigation bar*/
  .topnav {
    padding-bottom: 50px;
  }

  /* Style the links inside the navigation bar */
  .menu a {
    float: left;
    text-align: center;
    padding: 14px 16px;
    text-decoration: none;
    font-size: 20px;
    color: #636b6f;
    font-weight: bold;
    position: relative;
    left: 200px;
    text-transform: lowercase;
  }

  /* Change the color of links on hover */
  .topnav a:hover {
    color: rgb(166, 113, 237);
  }

  /*add no decoration to a button-link*/
  .linknodec {
    text-decoration: none;
    color: black;
  }


  .viewpost {
    color: rgb(66, 134, 244);
    text-decoration: none;
    
  }
</style>
</head>
<body>

<div class="topnav">
<div class="menu">
  <a href=/homepage>Home</a>
  <a href="/dashboard">Dashboard</a>
  <a href="/users">Userbase</a>
  <a href="/posts">View all posts</a>
  @if(Auth::check())
  <a href="{{ url('/logout') }}"> Log out </a>
  @endif
  <a href="/about">About</a>
</div>

  

<div class="login-container">

@if(Auth::check())
  <div class="loggedinuser" >
      <img class="tinyavatar" src= "{{ Auth::user()->avatar }}">
        {{ Auth::user()->username }}
      <input class="button" type="submit" name="submit" value="Log out" onclick="location.href='{{ url('/logout') }}'">    
  </div>

@else
  <form method="POST" action="{{ route('login') }}">
    @csrf
      @if(!Auth::check())
      
      <a href="/users/create">No account? Register!</a>
      @endif
      <input  type="text" placeholder="Enter Email adress" name="email" required>
      <input  type="password" placeholder="Enter Password" name="password" required>
      <input class="button" type="submit" name="submit" value="Login">    
      
    </form>
@endif
    
</div>
</div>
</div>

@yield('content')
<center>
</center>
</body>
</html>