<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Project0</title>
<!-- Fonts -->
<link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">

<!-- Styles -->
<style>
  html, body {
    background-color: #fff;
    color: #636b6f;
    font-family: 'Quicksand', sans-serif;
    font-weight: 200;
    height: 100vh;
    margin: 0;
    background-image: url("http://alliswall.com/file/6621/1920x1200/16:9/pink_mountains.jpg");
    background-size: cover;
    background-color: rgba(238, 238, 238, 0.7) !important;
    background-blend-mode: color;
  }
  .full-height {
      height: 100vh;
  }
  .flex-center {
      align-items: center;
      display: flex;
      justify-content: center;
      

   }
  .position-ref {
      position: relative;
  }
  .top-right {
      position: absolute;
      right: 10px;
      top: 18px;
  }
  .content {
    text-align: center;
    background-color:rgba(255,255,255,0.4);
    width: 1000px;
    height: 300px;
    padding: 10px;
    padding-top: 20px;
    border-radius: 12px;
    margin-top: 10px;
    padding-top: 50x;
    
  }
  .title {
      font-size: 85px;
      text-transform: uppercase;
        }
  .links > a {
      color: #636b6f;
      padding: 0 25px;
      font-size: 13px;
      font-weight: 600;
      letter-spacing: .1rem;
      text-decoration: none;
      text-transform: uppercase;
  } 
  .m-b-md {
      margin-bottom: 30px;
  }

  img {
    opacity: 0.6; 
    width: 100px;
  }

</style>
    </head>
    <body>
           <div class="flex-center position-ref full-height">
            
                <div class="top-right links">
                   
                </div>
          
            <div class="content">
            <img src="https://cdn.shopify.com/s/files/1/2378/1785/files/zero_3_250x.png?v=1531290588">
                <div class="title m-b-md">
                    project zero
                </div>

                <div class="links">
                    <a href="/users">Users</a>
                    <a href="/groups">Groups</a>
                    <a href="/about">About</a>
                    <a href="/login">Login</a>
                   
                </div>
            </div>
        </div>
        </div>
    </body>
</html>
