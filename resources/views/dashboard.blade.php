@extends('layout')
  <style>
    .block {
    margin-top: 15px;
    } 
</style>
@section('title', 'Dashboard')
@section('content')
<center>
<div class="block">
  <h2>Dashboard</h2>

@if(Auth::guest())
  <p>You need to <a href="\login">sign in</a> or <a href="\users/create">register</a> to view your dashboard!</p>

@else
  <img class="avatarbig" src=" {{ Auth::user()->avatar }}">
  <h3>Welcome back, {{ Auth::user()->username }}</h3>
  <button class="button"><a class="linknodec" href="/users/edit">Edit profile</a></button>
  <button class="button"><a class="linknodec" href="/posts/create">+ Create new post</a></button>

<h3>My latest posts</h3>

@foreach ($posts as $post) 

<table class="post">
  <div class="usericon">
    <tr>
      <td><img class="useravatar" src="{{ $post->user->avatar }}" ></td>
    </tr>

    <tr>
      <td class="username">{{ $post->user->username }}</td> 
    </tr>
  </div>

    <tr class="posttitle">
      <td>{{ $post->title  }}</td>
    </tr>

  <div class= "message">
    <tr>
      <td>{{ $post->message }}</td>
    </tr>
  </div>
    <tr>
      <td><a class="viewpost" href="/posts/{{ $post->id }}">View my post and comment</a></td>
    </tr>
    <tr>
      <td class="date">Posted at {{ $post->created_at }}</td>
    </tr>

    
@endforeach
@endif
</center>
</div>
@endsection