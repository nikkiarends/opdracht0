@extends('layout')
@section('title', 'About')

@section('content')
<center>
<div class="block">
<h2>Edit user profile</h2>

<form method="post" action="">
	@csrf
    <table>
        <tr>
            <td>Current username: {{ $user->username }}</td>
        </tr>

        <tr>
            <td>New username: </td>
        </tr>

        <tr>
            <td><input type="text" class="title" name="username" placeholder="Username" value="{{ $user->username}}"></td>
        </tr>

        <tr>
            <td>Current avatar:</td>
        </tr>

        <tr>
            <td> <img class="avatarbig" src="{{ $user->avatar }}"></td>
        </tr>
            
        <tr>
            <td>New avatar: </td>
        </tr>

        <tr>
            <td><input type="text" class="title" name="avatar" placeholder="Avatar URL" value="{{ $user->avatar}}"></td>
        </tr>

        @can('update', $user)
        <tr>
        <td>Group : </td>
        </tr>
        <tr>
            <td>
                <select class="select" name="group">
                    @foreach ($groups as $group) 
                        <option value="{{ $group->id }}">{{ $group->name }}</option>
                     @endforeach
            </td>
        </tr>
         @endcan
         
        <tr>
            <td><input class="button" type="submit" name="submit" value="Submit"><td>
        </tr>
   
</table>	
</div>
</center>
</div>

@endsection