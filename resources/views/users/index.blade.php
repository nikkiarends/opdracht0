@extends('layout')
<style>
.users {
  background-color:rgba(255,255,255,0.4);
  width: 1000px;
  padding: 10px;
  padding-top: 20px;
  border-radius: 12px;
  margin-top: 15px;
  border: 1px rgb(255, 102, 102, 0.1) solid;
  position: relative;
}

.bigavatar {
  width: 100px;
  height: 100px;
  padding: 5px;
  border-radius: 12px;
}

.name {
  padding: 5px;
  padding-left: 10px;
}

.date {
    font-size: 14px;
    text-align: center;
    color: salmon;
}
</style>
@section('title', 'Users')
@section('content')
<center>

<div class="block">
<h2>Users</h2>

@foreach ($users as $user) 

<table class="users">
<tr>
<td><img class="bigavatar" src= "{{ $user->avatar }}"></td>
</tr>
<tr>
<td class="name">{{ $user->username }}</td>
</tr>

<tr>
<td class="name"> {{ $user->groups[0]->name }}</td>
</tr>

<tr>
<td class="date">User registered at: {{ $user->created_at }}</td>
</tr>
@can('delete', $user)
<tr>
        <td><a href="{{ route('user.delete', ['id' => $user->id]) }}">
                        <button type="button" class="button">Delete User</button>
                    </a><td>
    </tr>

@endcan

@can('update', $user)
<tr>
        <td><a href="{{ route('user.edit', ['id' => $user->id]) }}">
                        <button type="button" class="button">Edit User</button>
                    </a><td>
    </tr>
@endcan

</table>

@endforeach
</div>
</center>

@endsection