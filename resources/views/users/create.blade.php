@extends('layout')
<style>
</style>
@section('title', 'Create User')
@section('content')
<center>
<div class="block">
<h2>Register</h2>

<form action="/users" method="POST">

@csrf

@if ($errors->any())
    <div class="notification">
        <ul>
        @foreach ($errors->all() as $error)
            <li style="list-style-type:none;"><strong>Warning! </strong>{{ $error }}</li>
        @endforeach
        </ul>
    </div>
@endif

<table>

    <tr>
        <td>Username : </td>
        <td><input type="text" name="username"><td>
    </tr>

    <tr>
        <td>Email adress : </td>
        <td><input type="text" name="email"><td>
    </tr>

    <tr>
        <td>Password : </td>
        <td><input type="password" name="password"><td>
    </tr>

     <tr>
         <td>Avatar URL : </td>
         <td><input type="text" name="avatar" ><td>

    </tr>     
    <tr>
        <td><input class="button" type="submit" name="submit" value="Submit"><td>
    </tr>
</table>
</form>
</center>
</div>
@endsection