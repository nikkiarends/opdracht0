@extends('layout')
<style>
.message {
    height: 200px;
    width: 600px;
    font-family: 'Quicksand', sans-serif;
    padding: 12px 20px;
    margin: 5px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
    border-radius: 12px;
     }
.title {
    width: 400px;
}

.button {
    width: 200px;
}


</style>
@section('title', 'Comment')
@section('content')
<center>
<div class="block">
<h2>Comment</h2>
@if(Auth::guest())
<p>You need to <a href="\login">sign in</a> or <a href="\users/create">register</a> to comment!</p>

@else

<form action="/posts/{{ $postid }}/comment" method="POST" id="messageform">

@csrf

<table>
<tr>
        <td>Title: </td>
        </tr>
        <tr>
        <td><input class="title" type="text" name="title"><td>
    </tr>
<tr><td>Message : </td></tr>
    <tr>
        
      <td>
      <textarea class="message" rows="4" cols="50" name="message" form="messageform"></textarea>
         </td> </tr>
    
    <tr>
        <td><input class="button" type="submit" name="submit" value="Add"><td>
    </tr>

   
</table>
</form>
</center>
</div>
@endif
@endsection