@extends('layout')
@section('title', 'Create group')
@section('content')
<center>
<div class="block">
<h2>Create Group</h2>

<form action="/groups" method="POST">

@csrf

<table>
    <tr>
        <td>Name : </td>
        <td><input type="text" name="name"><td>
    </tr>

    <tr>
        <td>Description : </td>
        <td><input type="text" name="description"><td>
    </tr>
    
    <tr>
        <td><input class="button" type="submit" name="submit" value="Add"><td>
    </tr>
</table>
</form>
</center>
</div>
@endsection