<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public function permissions() {
        return $this->belongsToMany (Permission::class, 'group_permission');
    }

    public function givePermissionTo(Permission $permission)
    {
        return $this->permissions()->save($permission);
    }

    public function hasPermission(Permission $permission, User $user)
    {
        return $this->hasGroup($permission->groups);
    }

    public function inGroup($permission)
    {
        if (is_string($permission)) {
            return $this->permissions->contains('name', $permission);
        }
        return !! $permission->intersect($this->permissions)->count();
    }

    public function users() {
        return $this->hasMany(User::class);
    }

    
}