<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    //view all users
    public function index() {
            $users = \App\User::all();
        return view('users/index', ['users' => $users]);
    }

    //create new user
    public function create () {
            $groups = \App\Group::all();
        return view ('users/create', ['groups' => $groups]);
    }
    
    //save the new user
    public function store (Request $request) {
        //check if everything required is entered and if username and email are unique
                $request->validate([
                'username' => 'required|unique:users,username|max:10|min:3',
                'email' => 'required|email|unique:users',
                'password' => 'required|max:10|min:5',       
                ]);
                $user = new \App\User();
                $user->username = request('username');
                $user->email = request('email');
                $user->password = bcrypt(request('password'));
                $user->avatar = request('avatar');
                $user->save();
                $user->groups()->attach(3);
            return redirect('/login');
        }
    //edit user
    public function edituser() {
        if (Auth::guest()) 
        return redirect('/users');
        else

            $user = Auth::user();
            $groups = \App\Group::all();
        return view('users/edit', ['user' => $user, 'groups' => $groups]);
    }

    //update edited user
    public function updateuser(Request $request) {
            $user = Auth::user();
            $user->username = $request->username;
            $user->avatar = $request->avatar;
            $user->save();
            $user->groups()->sync([$request->input('group')]);
        return redirect('/dashboard');
    }

    public function adminedituser($id) {
        if (Auth::guest()) 
        return redirect('/users');
        else
            $user= \App\User::find($id);
            $authuser = Auth::user();
            $groups = \App\Group::all();
            $authuser = Auth::user();
            abort_unless($authuser->can('delete', $post), 403);
        return view('users/edit', ['user' => $user, 'groups' => $groups]);
    }
   
    public function adminupdateuser(Request $request, $id) {
            $user= \App\User::find($id);
            $user->username = $request->username;
            $user->avatar = $request->avatar;
            $user->save();
            $user->groups()->sync([$request->input('group')]);
        return redirect('/users');
    }
    //delete user
    public function deleteuser($id) {
        if (Auth::guest()) 
        return redirect('/users');
        else
            $user= \App\User::find($id);
            $authuser = Auth::user();
            abort_unless($authuser->can('delete', $post), 403);
            $user->delete();
        return redirect('/users');
                
    }
    

}
