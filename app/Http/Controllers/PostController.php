<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class PostController extends Controller
{

    //Posts//

    //view all posts
    public function index() { 
        $posts = \App\Post::where('post_id', null)->orderByDesc("created_at")->get();
        return view('posts/index', ['posts' => $posts]);
    }
    //create a new post
    public function create () {
        return view ('posts/create');
    }
    
    //save the new post
    public function store () {
            request()->validate([
                'title' => 'required',
                'message' => 'required',
            ]);

            $post = new \App\Post();
            $id = Auth::id();
            $post->title = request('title');
            $post->message = request('message');
            $post->user_id = $id;
            $post->save();
        return redirect('/posts');
    }
    //edit post
    public function editpost($id) {
        if (Auth::guest()) 
        return view('/auth/login');
        
        else
            $post = \App\Post::find($id);
            $postid = $post->post_id;
            $authuser = Auth::user();
            abort_unless($authuser->can('delete', $post), 403);
        return view('posts/edit', ['post' => $post]);  
    }

    //update edited post
    public function updatepost(Request $request, $id) {
        if (Auth::guest()) 
        return view('/auth/login');
        else

            $post = \App\Post::find($id);
            $post->title = $request->title;
            $post->message = $request->message;
            $post->save();
        return redirect()->route('posts', ['id' => $id]);
    }

    //delete post
    public function deletepost($id) {
        if (Auth::guest()) 
        return view('/auth/login');
        else
            $post = \App\Post::find($id);
            $authuser = Auth::user();
            abort_unless($authuser->can('delete', $post), 403);
            $post->delete();
        return redirect('/posts');
    }

    //view user's posts
    public function myposts () {
        $posts = \App\Post::where('user_id', auth()->id())->orderByDesc("created_at")->get();
        return view('dashboard', ['posts' => $posts]);
    }

    //view a certain post on a new page
    public function viewpost(\App\Post $post) {
        return view('/posts/postview', ['post' => $post]);
    }

    //Comments//

    //create commment
    public function comment ($postid) {
        return view('/comments/create', ['postid'=> $postid]);
    }

    //store comment
    public function storecomment($postid) {
            $post = new \App\Post(); //create new comment
            $userid = Auth::id();
            
            $post->title = request('title');
            $post->message = request('message');
            $post->user_id = $userid;
            $post->post_id = $postid;
            $post->save();
        return redirect()->route('posts', ['id' => $postid]);
    }

    //delete comment
    public function deletecomment($id) {
            $post = \App\Post::find($id);
            $authuser = Auth::user();
            $postid = $post->post_id;
            abort_unless($authuser->can('delete', $post), 403);      
            $post->delete(); 
        return redirect()->route('posts', ['id' => $postid]);
        
        
    }
}
