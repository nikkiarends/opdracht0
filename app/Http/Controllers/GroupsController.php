<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GroupsController extends Controller
{
    //Group overview
    public function index() {
      $groups = \App\Group::all();
     return view('groups/index', ['groups' => $groups]); 
    }

    //create new group
    public function create () {
        return view ('groups/create');
    }

    public function store () {
    $group = new \App\Group();
    $group->name = request('name');
    $group->description= request('description');
    $group->save();

    return redirect('/groups');
        
    }
}
