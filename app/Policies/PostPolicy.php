<?php

namespace App\Policies;

use App\User;
use App\Post;
use App\Permission;
use App\Group;
use Illuminate\Auth\Access\HandlesAuthorization;


class PostPolicy
{
    use HandlesAuthorization;

    
    public function update(User $user, Post $post)
    {
        $permission = Permission::where('name', 'Can edit posts')->first();    
        return $post->user_id == $user->id ||  $user->hasGroup($permission->groups); //checks if user is owner or if has group permissions
    }

   
    public function delete(User $user, Post $post)
    {
        $permission = Permission::where('name', 'Can delete posts')->first();
        return $post->user_id == $user->id || $user->hasGroup($permission->groups);
    }

}
