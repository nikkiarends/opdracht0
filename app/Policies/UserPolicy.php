<?php

namespace App\Policies;

use App\User;
use App\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function delete(User $user)
    {
        $permission = Permission::where('name', 'Can delete users')->first();
        return $user->hasGroup($permission->groups);
    }

    public function update(User $user)
    {
        $permission = Permission::where('name', 'Can edit users')->first();    
        return $user->hasGroup($permission->groups); 
    }


}
