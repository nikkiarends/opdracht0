<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    public function groups() {
    return $this->belongsToMany(Group::class, 'group_permission');
}

public function inGroup($group)
{
    if (is_string($group)) {
        return $this->groups->contains('name', $group);
    }
    return !! $group->intersect($this->groups)->count();
}
}
