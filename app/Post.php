<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function user () {
        return $this->belongsTo(User::class);
    }

    public function post () {
        return $this->belongsTo(Post::class);
    }

    public function comments () {
        return $this->hasMany(Post::class);
    }

}
