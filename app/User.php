<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
//use \App\Permissions\hasPermissionsTrait;


class User extends Authenticatable
{
    use Notifiable;
    //use hasPermissionsTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function groups() {
        return $this->belongsToMany(Group::class);
    }

    public function posts()
    {
      return $this->hasMany(Post::class);
    }
    
    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }




    public function assignGroup(Group $group)
    {
        return $this->groups()->save($group);
    }

    public function hasGroup($group)
    {
        if (is_string($group)) {
            return $this->groups->contains('name', $group);
        }
        return !! $group->intersect($this->groups)->count();
    }
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
